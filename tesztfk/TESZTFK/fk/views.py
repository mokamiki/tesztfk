from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout


@login_required(login_url='login/')
def index(request):
    return render(request, 'fk/opened.html')


@login_required(login_url='login/')
def newissue(request):
    return render(request, 'fk/newissue.html')


@login_required(login_url='login/')
def late(request):
    return render(request, 'fk/late.html')


@login_required(login_url='login/')
def suspended(request):
    return render(request, 'fk/suspended.html')


@login_required(login_url='login/')
def petition(request):
    return render(request, 'fk/petition.html')


@login_required(login_url='login/')
def department(request):
    return render(request, 'fk/department.html')


@login_required(login_url='login/')
def closed(request):
    return render(request, 'fk/closed.html')


def logout_view(request):
    logout(request)
    return render(request, 'registration/logged_out.html')
