from django.db import models


class Issue(models.Model):
    issue_pk = models.CharField(max_length=5, unique=True)
    registration_number = models.CharField(max_length=30)
    subject = models.CharField(max_length=50)
    user_full_name = models.CharField(max_length=50)
    som_date = models.DateField()
    date = models.DateField()

    def __str__(self):
        return self.registration_number


    class Meta:
        permissions = (
            ("can_add_new_issue", "Add new issue"),
        )
