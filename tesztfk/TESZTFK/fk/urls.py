from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('newissue/', views.newissue, name='newissue'),
    path('opened/', views.index, name='index'),
    path('suspended/', views.suspended, name='suspended'),
    path('petition/', views.petition, name='petition'),
    path('department/', views.department, name='department'),
    path('late/', views.late, name='late'),
    path('closed/', views.closed, name='closed'),
    path('logout/', views.logout_view),
]
